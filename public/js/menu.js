$(function() {
    $('body').removeClass('nav-md').addClass('nav-sm');
    $('.left_col').removeClass('scroll-view').removeAttr('style');
    $('#sidebar-menu li').removeClass('active');
    $('#sidebar-menu li ul').slideUp();
});