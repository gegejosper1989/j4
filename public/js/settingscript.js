$(document).ready(function() {
    $(document).on('click', '.save-percent', function() {
        $.ajax({
            type: 'post',
            url: '/admin/settings/percentage/save',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'percentval': $('#percentval').val(),
                'setid': $('#setid').val()
            },
            success: function(data) {
                $("#myModal").modal();
                // setTimeout(function(){ 
                //         location.reload();
                //     }, 3000);
            }
        });
    });

    $(document).on('click', '.close-btn', function() {
        location.reload();
    });

    $(document).on('click', '.closeSale', function() {
        $('#cid').val($(this).data('cid'));
        $('#cashierid').val($(this).data('cashierid'));
        $("#cproductimg").attr("src",'/productimg/'+$(this).data('pic'));
        $('#cproductedit_name').text($(this).data('cname'));
        $('#cproductamount').text($(this).data('camount'));
        $('#camount').val($(this).data('camount'));
        $('#cquantity').val($(this).data('cquantity'));
        $('#closeDailySaleModal').modal('show');
        //console.log($(this).data('cashierid'));
        //console.log($(this).data('name') + $(this).data('points'));
    });
    $('.modal-footer').on('click', '.close-dailysale', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/sale/close',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'dailysales': $("#dailysales").val(),
                'totaldailysales': $("#totaldailysales").val(),
                'grossprofit': $("#grossprofit").val(),
                'expenses': $('#expenses').val(),
                'netprofit': $('#netprofit').val()
            },
            success: function(data) {
              //location.redirect();
              window.location.replace('/admin/dailysale/' + data.id);
            }
        });
    });
});
  