$(document).ready(function () {
    //@naresh action dynamic childs
    var next = 0;
    $("#add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn =`<div id="field${next}'" name="field${next}">
        <div class="form-group col-md-3">
            <label class="control-label" for="Variation">Variation Type</label>  
                <div class="">
                    <input id="vartype" name="vartype[]" type="text" placeholder="" class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group col-md-3">
                <label class="control-label" for="Ordering">Ordering Point</label>  
                <div class="">
                    <input id="orderpoint" name="orderpoint[]" type="number" placeholder="" class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group col-md-3">
                <label class="control-label" for="Price">Price</label>  
                <div class="">
                <input id="varprice" name="varprice[]" type="number" placeholder="" value="0" class="form-control input-md" required>
                </div>
            </div>

            <div class="form-group col-md-2">
                <label class="control-label" for="Stocks">Stocks</label>  
                <div class="">
                <input id="varstocks" name="varstocks[]" type="number" placeholder="" value="0" class="form-control input-md" required>
                </div>
            </div>
        </div>`;
        var newInput = $(newIn);
        var removeBtn = '<div class="form-group col-md-1" id="remove' + (next - 1) + '"><label for="">&nbsp;</label><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" ><i class="fa fa-close"></i></button></div></div>';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                var removeID = "#remove" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
                $(removeID).remove();
            });
    });
    $("#update-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = `<div id="field${next}" name="field${next}">
                        <div class="form-group col-md-3">
                        <label class="control-label" for="Variation">Variation Type</label>  
                            <div class="">
                                <input id="vartype" name="vartype[]" type="text" placeholder="" class="form-control input-md" required>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label" for="Ordering">Ordering Point</label>  
                            <div class="">
                                <input id="orderpoint" name="orderpoint[]" type="number" placeholder="" class="form-control input-md" required>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label class="control-label" for="Price">Price</label>  
                            <div class="">
                            <input id="varprice" name="varprice[]" type="number" placeholder="" value="0" class="form-control input-md" required>
                            </div>
                        </div>

                        <div class="form-group col-md-2">
                            <label class="control-label" for="Stocks">Stocks</label>  
                            <div class="">
                            <input id="varstocks" name="varstocks[]" type="number" placeholder="" value="0" class="form-control input-md" required>
                            </div>
                        </div>
                    </div>`;
        var newInput = $(newIn);
        var removeBtn = '<div class="form-group col-md-1" id="remove' + (next - 1) + '"><label for="">&nbsp;</label><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" ><i class="fa fa-close"></i></button></div></div>';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                var removeID = "#remove" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
                $(removeID).remove();
            });
    });

    $(document).on('click', '.edit-product', function() {
        $('#footer_action_button').text("Update");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Edit Variation');
        $('.deleteContent').hide();
        $('.form-horizontal').show();
        $('#fid').val($(this).data('id'));
        $('#optionid').val($(this).data('optionid'));
        $('#editvartype').val($(this).data('optionname'));
        $('#editvarprice').val($(this).data('varprice'));
        $('#editvarstocks').val($(this).data('warehousequantity'));
        $('#myModal').modal('show');
        //console.log($(this).data('name') + $(this).data('points'));
    });
    $('.modal-footer').on('click', '.edit', function() {
  
        $.ajax({
            type: 'post',
            url: '/admin/products/updateproductvariation',
            data: {
                //_token:$(this).data('token'),
                '_token': $('input[name=_token]').val(),
                'id': $("#fid").val(),
                'optionid': $("#optionid").val(),
                'vartype': $('#editvartype').val(),
                'varprice': $('#editvarprice').val(),
                'varstocks': $('#editvarstocks').val()
            },
            success: function(data) {
                alert("Variation Updated!");
                location.reload();
            }
        });
    });

});