@extends('admin.layouts.admin')
@section('content')

<div class="row">
    <div class="col-md-4 col-sm-3 col-xs-12 no-print">
    <div class="x_panel">
                <div class="x_title">
                    <h2>Daily Sale
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">  
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Daily Sale</th>
                            <th></th>
                        </tr>
                        @foreach($dailysale as $sale)
                        <tr>
                            <td>{{$sale->created_at->format('m/d/Y')}}</td>
                            <td>{{number_format($sale->dailysales,2)}}</td>
                            <td>
                                <a href='/admin/dailysale/{{$sale->id}}' class="btn btn-sm btn-info"><i class="fa fa-search"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </thead>
                    </table>
                </div>        
        </div>
        <div class="x_panel">
                <div class="x_title">
                    <h2>Monthly Report
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">  
                    <form action="/admin/report/monthreport/generate" method="post">
                        {{ csrf_field() }}   
                        <div class="box-body">
                            <div class="form-group">
                            <label for="">From: </label>
                            <select name="month" class="form-control">
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select> 
                            </div>
                            <div class="form-group">
                            <label for="">Year: </label>
                                <select name="year" id="year" class="form-control">
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                </select>
                            </div>
                        </div>
                        <div class="" style="padding:20px;">
                            <button type="submit" class="pull-right btn btn-default" id="sendEmail">Generate Report <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </form> 
                </div>
                
         </div>
         <div class="x_panel">
                <div class="x_title">
                    <h2>Sales Report
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">  
                    <form action="/admin/report/generate" method="post">
                                {{ csrf_field() }}   
                                <div class="box-body">
                                        <div class="form-group">
                                        <label for="">Month: </label>
                                            <input type="date" class="form-control" name="from" required/>
                                        </div>
                                        <div class="form-group">
                                        <label for="">To: </label>
                                            <input type="date" class="form-control" name="to" required/>
                                        </div>
                                </div>
                                <div class="" style="padding:20px;">
                                    <button type="submit" class="pull-right btn btn-default" id="sendEmail">Generate Report <i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                    </form> 
                </div>
                
         </div>
      </div>
      <div class="col-md-8 col-sm-8 col-xs-12 col-lg-8">
        <div class="x_panel">
                  <div class="x_title">
                  <?php 
                  $monthNum  = $Month;
                  $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                  $monthName = $dateObj->format('F'); // March

                  $totalDailySale = 0;
                  $totalGrossDailySale = 0;
                  $totalGrossProfit = 0;
                  $totalExpenses = 0;
                  $totalNetProfit =0;
                  ?>
                    <h2>Monthly Sales Report for {{$monthName}} {{$Year}}
                    </h2>
                    <div class="clearfix"></div>
                  </div>
                <div class="x_content">   
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Daily Sales </th>
                          <th>Gross Daily Sales</th>
                          <th>Gross Profit</th>
                          <th>Expenses</th>
                          <th>Net Profit</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($dailysaleMonthly as $monthlySale)
                      <tr>
                        <td>{{$monthlySale->created_at->format('m/d/Y')}}</td>
                        <td>{{$monthlySale->dailysales}}</td>
                        <td>{{$monthlySale->grossdailysales}}</td>
                        <td>{{$monthlySale->grossprofit}}</td>
                        <td>{{$monthlySale->expenses}}</td> 
                        <td>{{$monthlySale->netprofit}}</td>
                      </tr>
                      <?php 
                        $totalDailySale = $totalDailySale + $monthlySale->dailysales;
                        $totalGrossDailySale = $totalGrossDailySale + $monthlySale->grossdailysales;
                        $totalGrossProfit = $totalGrossProfit + $monthlySale->grossprofit;
                        $totalExpenses = $totalExpenses + $monthlySale->expenses;
                        $totalNetProfit = $totalNetProfit + $monthlySale->netprofit;
                      ?>
                      @endforeach
                      </tbody>
                    </table>
                    <div class="col-xs-6 col-lg-5 col-md-4"></div>
                    <div class="col-xs-8 col-lg-7 col-md-8">
                        <p class="lead">Summary</p>
                        <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width:50%">Total Daily Sales:</th>
                                <td>{{number_format($totalDailySale,2)}}</td>
                            </tr>
                            <tr>
                                <th>Total Gross Daily Sales</th>
                                <td>{{number_format($totalGrossDailySale,2)}}</td>
                            </tr>
                            <tr>
                                <th>Total Gross Profit</th>
                                <td> {{number_format($totalGrossProfit,2)}}</td>
                            </tr>
                            <tr>
                                <th>Total Expenses:</th>
                                <td>{{number_format($totalExpenses,2)}}</td>
                            </tr>
                            <tr>
                                <th>Total Net Profit:</th>
                                <td>{{number_format($totalNetProfit,2)}}</td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="col-lg-12">
                    <button class="btn btn-primary hidden-print noprint" align="right" onclick="myFunction()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print</button>
                            <script>
                            function myFunction() {
                            window.print();}
                            </script>
                    </div> 
                </div>
         </div>
      </div>
</div>
@endsection