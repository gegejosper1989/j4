@extends('admin.layouts.admin')

@section('content')
<div class="row">
<div class="col-md-9 col-sm-9 col-xs-12">
        <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps anchor">
                        <li>
                          <a class="selected" isdone="1" rel="1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              Step 1<br>
                                              <small>Product Details</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a  class="selected" isdone="1" rel="2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Step 2<br>
                                              <small>Add Product Variation</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a  class="selected" isdone="1" rel="3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              Step 3<br>
                                              <small>Success</small>
                                          </span>
                          </a>
                        </li>
                        
                      </ul>
            </div>    
    </div>
</div>
<div class="row">
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
            @php
            Session::forget('success');
            @endphp
        </div>
    @endif
    <div class="col-md-12 col-sm-3 col-xs-12">
    
        <div class="x_panel">
				@foreach($dataProduct as $Product)  
				<div class="x_title">
                    <h3>Product Details
                    </h3>
                    <div class="clearfix"></div>
                  </div>
                <div class="x_content">     
					<h4 class="my-3">Product Name: {{$Product->product_name}}</h4>
                        <h5 class="my-3">Product Details</h5>
                        <ul>
                                
                                <li>Unit: <a class="name">{{$Product->unit}}</a></li>
                                <li>Bulk Quantity: <a class="name">{{$Product->bulkquantity}}</a></li>
                                <li>Bulk Unit: <a class="name">{{$Product->bulkunit}}</a></li>
                        </ul>
                        <table class="table table-striped">
                        <thead>
                            <tr>
                            
                            <th>Variations</th>
                            
                            <th>Stock</th>
                            <th>Price</th>
                            <th>Ordering Point</th>
            
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($dataProductVariantOptions as $ProductVariantOptions)
                            <tr>
                            <td>{{$ProductVariantOptions->varoption->option_name}}</td>
                            <td>{{$ProductVariantOptions->warehousequantity}}</td>
                            <td>{{$ProductVariantOptions->varprice}}</td>
                            <td>{{$ProductVariantOptions->orderingpoint}}</td>
                            </tr>
                            
                        @endforeach
                        </tbody>
                        </table>
				</div>
				@endforeach
		</div>
        <div class="actionBar"><a href="/admin/products/add/" class="buttonNext btn btn-success"><i class="fa fa-plus"> </i> Add New</a><a href="/admin/products/{{$productid}}" class="buttonFinish btn btn-info"><i class="fa fa-search"></i>View Product</a></div>
	</div>
   
        
	</div>

</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/variationscript.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.js"></script>
@endsection