@extends('admin.layouts.admin')

@section('content')
    <div class="row">
            <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                          <p class="lead">Daily Sale for {{$dataSale->created_at->format('m/d/Y')}}</p>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Daily Sale:</th>
                                  <td>{{$dataSale->dailysales}}</td>
                                </tr>
                                <tr>
                                  <th>Sale</th>
                                  <td>{{$dataSale->grossdailysales}}</td>
                                </tr>
                                <tr>
                                  <th>Gross Profit:</th>
                                  <td>{{$dataSale->grossprofit}}</td>
                                </tr>
                                <tr>
                                  <th>Expenses:</th>
                                  <td>{{$dataSale->expenses}}</td>
                                </tr>
                                <tr>
                                  <th>Net Profit:</th>
                                  <td>{{$dataSale->netprofit}}</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="col-xs-12 no-print">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          <a href="/admin/dashboard" class="btn btn-success pull-right"><i class="fa fa-reply"></i> Back</a>
                         
                        </div>
                  <div class="clearfix"></div>
                </div>
               
                
              </div>
              
            </div>
   
  
</div>
<script src="{{ asset('js/app.js') }}"></script>


<!-- /main -->
@endsection