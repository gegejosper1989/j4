@extends('admin.layouts.admin')
@section('content')

<div class="row">
      <div class="col-md-8 col-sm-8 col-xs-12 col-lg-9">
        <div class="x_panel">
                  <div class="x_title">
                    <h2>Delivery Report
                    </h2>
                    <div class="clearfix"></div>
                  </div>
                <div class="x_content">   
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Delivery Number</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $totalamount= 0;?>
                        @foreach($dataDelivery as $Delivery)
                        
                        <tr>
                            <td>{{$Delivery->deliverydate}}</td>
                            <td><a href="/admin/delivery/details/{{$Delivery->deliverynum}}">{{$Delivery->deliverynum}}</a></td>
                            <td><a href="/admin/delivery/details/{{$Delivery->deliverynum}}" class="btn btn-sm btn-success"><i class="fa fa-search"></i> View</a></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
         </div>
      </div>
</div>

@endsection