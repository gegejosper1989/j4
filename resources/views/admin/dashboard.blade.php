@extends('admin.layouts.admin')

@section('content')
    <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2>Recent Transactions</h2>
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>OR Number</th>
                        <th>Total Amount</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @forelse($reportPurchase as $Purchase)   
                      <tr>
                      <td align="center" style="text-align:center">{{$Purchase->date}}</td>
                        <td><a href="/admin/vieworder/{{$Purchase->orderNumber}}">{{$Purchase->ornumber}}</a></td>
                  
                        <td>{{number_format($Purchase->amount,2)}}</em>  </td>
                            <td>
                              <?php 
                              if($Purchase->status == 0){
                              echo "Accepted";
                              }
                              elseif($Purchase->status == 1){
                              echo "Cancelled";
                              }
                              else {
                              echo "Unknown";
                              }
                              ?>
                            </td>
                        </tr>
                        @empty
                        <tr><td colspan="4">No Data</td></tr>
                      @endforelse
                    </tbody>
                  </table>
                  <div style="float:right; padding-right:10px;"><a href="/admin/report" type="button" class="btn btn-info btn-xs" >+ View More</a></div>
                  <div class="clearfix"></div>
                </div>
               
                
              </div>
              <div class="x_panel tile ">
                <div class="x_title">
                  <h2>Out of Stocks</h2>
                  <div class="nav navbar-right panel_toolbox">
                      <a href="/admin/outofstocks" class="btn btn-info btn-xs" style="margin-top:10px;"> <i class="fa fa-plus"></i> View More</a>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
                  <table class="table table-striped ">
                      <thead>
                        <tr>
                          <th>Product Name</th>
                          <th>Variation</th>
                          <th>On Hand</th>
                          <th>Ordering Point</th>
                        </tr>
                      </thead>
                    <tbody>
                      @foreach($dataOutOfStock as $stockProduct)
                      <tr class="item{{$stockProduct->id}}">
                        <td><a href="/admin/products/{{$stockProduct->prod_id}}">{{ ucwords($stockProduct->product->product_name) }}</a></td>
                        <td>
                        {{ ucwords($stockProduct->productvariants->option_name) }}
                        </td>
                        <td>{{$stockProduct->quantity}}</td>
                        <td>{{$stockProduct->orderingpoint}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>   
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daily Sales
                    </h2>
                    <div class="clearfix"></div>
                </div>
                @if($reportDailysale == 0)
                <div class="x_content">
                    {{ csrf_field() }}
                    <h5>Sales of the Day : {{$DailySales}}</h5> <br>
                    <label for="Total Sales">Total Sales</label>
                    <?php 
                        $percentage = $dataSetting->percentagesale / 100;
                        $totalGross = $DailySales * $percentage;
                        $totalSales = $DailySales - $totalGross;
                       
                    ?>  
                    <input type="hidden" class="form-control" name="dailysales" value="{{$DailySales}}" id="dailysales">
                    <input type="text" class="form-control" name="totaldailysales" value="{{$totalSales}}" id="totaldailysales" readonly>

                    <label for="Gross Profit">Gross Profit</label>
                    <input type="text" class="form-control" name="grossprofit" id="grossprofit" value="{{$totalGross}}">
                    <label for="Expenses">Expenses</label>
                    <input type="text" class="form-control" name="expenses" id="expenses">
                    <label for="Net Profit">Net Profit</label>
                    <input type="text" class="form-control" name="netprofit" id="netprofit">
                    <br>
                    <input type="submit" class="btn btn-sm btn-warning closeSale" value="Close Daily Sale">
                </div> 
              @else
              <div class="x_content">
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    Your good to go, Sales report for today is already close. Another transactions must be inputed tomorrow.
                  </div>
              </div>
              @endif                        
              </div>
             
              <div class="x_panel">
              <label for="Percentage Sale">Percentage Sale</label>
                <div class="input-group col-lg-4">
                  @if(!empty($dataSetting))
                  <input type="hidden" class="form-control" value="{{$dataSetting->id}}" id="setid">
                  <input type="text" class="form-control" value="{{$dataSetting->percentagesale}}" id="percentval">
                  @else
                  <input type="text" class="form-control" value="0" id="percentval">
                  @endif
                  <span class="input-group-btn">
                      <button type="button" class="save-percent btn btn-primary">Save</button>
                  </span>
                </div>
                            
              </div>
             
            </div>

          </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Save success</h4>
        </div>
        <div class="modal-body">
          <p>Percentage sale save!</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="close-btn btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="closeDailySaleModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title warning">Daily Sale</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure to close your daily sales for today?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
          <button type="button" class="close-dailysale btn btn-success" data-dismiss="modal">Close Sale</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/settingscript.js') }}"></script>
<script>
$(function() {
    $('#expenses').keyup(function() {  
        updateTotal();
    });
    const updateTotal = function () {
      let expenses = parseFloat($('#expenses').val()).toFixed(2);
      let grossprofit = parseFloat($('#grossprofit').val()).toFixed(2);
      let netprofitval = grossprofit - expenses;
      $('#netprofit').val(parseFloat(netprofitval).toFixed(2));
    };

    $('#dailysales').keyup(function() {  
        updateTotalSales();
    });
    const updateTotalSales = function () {
      let dailysales = parseFloat($('#dailysales').val()).toFixed(2);
      let percentage = {{ $dataSetting->percentagesale }} / 100;
      let totalGross = dailysales * percentage;
      let totalSales = dailysales - totalGross;
      //let netprofitval = grossprofit - expenses;
      $('#totaldailysales').val(parseFloat(totalSales).toFixed(2));
      $('#grossprofit').val(parseFloat(totalGross).toFixed(2));
      
    };
 });
</script>
<!-- /main -->
@endsection