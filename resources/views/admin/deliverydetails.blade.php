@extends('admin.layouts.admin')

@section('content')


<div class="row">       
      <div class="col-md-8 col-sm-8 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
            <h2>Delivery Record # {{$deliverynum}}
            </h2>
            <div class="clearfix"></div>
            </div>
                  <div class="x_content">

                    <section class="content invoice">
                        <table class="table table-striped table-bordered" >
                          <thead>
                            <tr>
                              <td>Date</td>
                              <th>Product Name</th>
                              <th>Product Variation</th>
                              <th>Quantity</th>
                             
                            </tr>
                          </thead>
                          <tbody>

                            @forelse($dataDeliverydetail as $Deliverydetail)
                            <tr>

                                <td>{{$Deliverydetail->updated_at->format('m/d/Y')}}</td>
                              <td>{{$Deliverydetail->product->product_name}}</td>
                              <td>{{$Deliverydetail->variation->option_name}}</td>
                              <td><em class="productprice">{{$Deliverydetail->quantity}}</em>  </td>


                            </tr>
                            @empty
                            <tr><td colspan='4'><em>No Data</em></td></tr>
                            @endforelse
                            
                            <?php $delDate = $Deliverydetail->updated_at; ?>
                          
                          </tbody>
                        </table>
                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                          <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
      </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<!-- /main -->
@endsection