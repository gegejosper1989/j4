@extends('admin.layouts.admin')

@section('content')
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              
                <div class="x_panel tile ">
                <div class="x_title">
                  <h2>Out of Stocks</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">
                  <table class="table table-striped ">
                      <thead>
                        <tr>
                          <th>Product Name</th>
                          <th>Variation</th>
                          <th>On Hand</th>
                          <th>Ordering Point</th>
                          
                        </tr>
                      </thead>
                    <tbody>
                      @foreach($dataOutOfStock as $stockProduct)
                      <tr class="item{{$stockProduct->id}}">
                        <td><a href="/admin/products/{{$stockProduct->prod_id}}">{{ ucwords($stockProduct->product->product_name) }}</a></td>
                        <td>
                        {{ ucwords($stockProduct->productvariants->option_name) }}
                        </td>
                        <td>{{$stockProduct->quantity}}</td>
                        <td>{{$stockProduct->orderingpoint}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>   
                  </div>
                </div>
              </div>
        </div>
        <div class="col-lg-12">
                    <button class="btn btn-primary hidden-print noprint" align="right" onclick="myFunction()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print</button>
                            <script>
                            function myFunction() {
                            window.print();}
                            </script>
                    </div> 
        </div>
        <!-- /span4 --> 
      </div>
      <!-- /row --> 

    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/productscript.js') }}"></script>
<script src="{{ asset('js/itemscript.js') }}"></script>
<!-- /main -->
@endsection