@extends('admin.layouts.admin')

@section('content')
<div class="row">
        <div class="col-md-4 col-sm-3 col-xs-6">
            <div class="x_panel">
                  <div class="x_title">
                    <h2>Supplier
                    </h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    {{ csrf_field() }}
                    <fieldset>	                                        
                        <div class="input-group">
                        <label for="Supplier">Supplier Name</label>
                        <input type="text" class="form-control" placeholder="Supplier Name"  aria-describedby="basic-addon2" name="supplier_name">
                        </div>
                        <div class="input-group">
                        <label for="Supplier Address">Supplier Address</label>
                        <input type="text" class="form-control" placeholder="Supplier Address"  aria-describedby="basic-addon2" name="supplier_address">
                        
                        </div>
                        <button class="btn btn-primary" type="submit" id="add">Save</button>   
                    </fieldset>    
                  </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-4 col-xs-6">
            <div class="x_panel">
                      <div class="x_title">
                        <h2>Supplier List
                        </h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                      <table class="table table-striped table-bordered" id="table">
                        <thead>
                          <tr>
                            <th> Name</th>
                            <th> Address</th>
                            <th> Action </th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($data as $Supplier)
                          <tr class="item{{$Supplier->id}}">
                            <td> <a class="name">{{$Supplier->supplier_name}}</a> </td>
                            <td> <a class="name">{{$Supplier->supplier_address}}</a> </td>
                            <td class="td-actions"><a href="javascript:;" class=" edit-modal btn btn-small btn-success" data-id="{{$Supplier->id}}" data-name="{{$Supplier->supplier_name}}" data-address="{{$Supplier->supplier_address}}"><i class="fa fa-pencil"> </i></a><a href="javascript:;" class="delete-modal btn btn-danger btn-small" data-id="{{$Supplier->id}}" data-name="{{$Supplier->supplier_name}}" data-address="{{$Supplier->supplier_address}}"><i class="fa fa-times"> </i></a></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      </div>
              </div>
        </div>
</div>
<!-- /main -->
<div id="myModal" class="modal fade" role="dialog">
  		<div class="modal-dialog">
  			<!-- Modal content-->
  			<div class="modal-content">
  				<div class="modal-header">
  					<button type="button" class="close" data-dismiss="modal">&times;</button>
  					<h4 class="modal-title"></h4>
  				</div>
  				<div class="modal-body">
  					<form class="form-horizontal" role="form">
                      {{ csrf_field() }}
  						<div class="form-group">
  							<label class="control-label col-sm-2" for="id">ID:</label>
  							<div class="col-sm-10">
  								<input type="text" class="form-control" id="fid" disabled>
  							</div>
  						</div>
  						<div class="form-group">
  							<label class="control-label col-sm-2" for="supplier_name" >Supplier Name:</label>
  							<div class="col-sm-10">
  								<input type="text" class="form-control" id="supplieredit_name" >
  							</div>
                <label class="control-label col-sm-2" for="supplier_name" >Supplier Address:</label>
  							<div class="col-sm-10">
  							
                  <input type="text" class="form-control" id="supplieredit_address">
  							</div>
                          </div>
            
  					</form>
  					<div class="deleteContent">
  						Are you Sure you want to delete <span class="dname"></span> ? <span
  							class="hidden did"></span>
  					</div>
  					<div class="modal-footer">
  						<button type="button" class="btn actionBtn" data-dismiss="modal">
  							<span id="footer_action_button" class='glyphicon'> </span>
  						</button>
  						<button type="button" class="btn btn-warning" data-dismiss="modal">
  							<span class='glyphicon glyphicon-remove'></span> Close
  						</button>
  					</div>
  				</div>
  			</div>
		  </div>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
    </div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/supplierscript.js') }}"></script>
@endsection