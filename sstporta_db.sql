-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2019 at 10:20 AM
-- Server version: 5.7.28-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sstporta_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `branch_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Aurora', NULL, '2018-05-30 14:37:11', '2018-03-11 03:13:10'),
(5, 'Storm Resources Trading', NULL, '2019-03-31 14:18:18', '2019-03-31 14:18:18'),
(6, 'Cebu Auto Supply', NULL, '2019-03-14 00:36:02', '2019-03-14 00:36:02'),
(7, 'Cebu Campirong', NULL, '2019-03-14 00:36:20', '2019-03-14 00:36:20'),
(8, 'Davao', NULL, '2019-03-14 00:36:31', '2019-03-14 00:36:31'),
(9, 'Manila', NULL, '2019-03-14 00:53:31', '2019-03-14 00:53:31'),
(10, 'SRT', NULL, '2019-03-31 14:18:57', '2019-03-31 14:18:57');

-- --------------------------------------------------------

--
-- Table structure for table `branchusers`
--

CREATE TABLE `branchusers` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branchusers`
--

INSERT INTO `branchusers` (`id`, `branch_id`, `userid`, `created_at`, `updated_at`) VALUES
(1, 4, 54, '2019-03-14 01:26:01', '2019-03-14 01:26:01'),
(2, 4, 55, '2019-03-14 01:26:45', '2019-03-14 01:26:45'),
(3, 5, 58, '2019-03-14 02:58:14', '2019-03-14 02:58:14'),
(4, 5, 59, '2019-03-14 02:58:43', '2019-03-14 02:58:43'),
(5, 8, 60, '2019-03-14 03:08:53', '2019-03-14 03:08:53'),
(6, 8, 61, '2019-03-14 03:09:09', '2019-03-14 03:09:09');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Steel Asia', NULL, '2019-03-14 00:54:40', '2019-03-31 13:30:32'),
(2, 'Boysen', NULL, '2019-03-31 13:28:15', '2019-03-31 13:33:39'),
(3, 'Island', NULL, '2019-03-31 13:28:42', '2019-03-31 13:33:33'),
(4, 'APO', NULL, '2019-03-31 13:29:10', '2019-03-31 13:33:48'),
(5, 'Federal', NULL, '2019-04-05 04:47:51', '2019-04-05 04:47:51'),
(6, 'Golden Bridge', NULL, '2019-04-05 04:55:24', '2019-04-05 04:55:24');

-- --------------------------------------------------------

--
-- Table structure for table `cancelorders`
--

CREATE TABLE `cancelorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cashierid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordernumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ornumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `prodquantityid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `requestId` int(11) NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cashierorders`
--

CREATE TABLE `cashierorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `cashier_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rquantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ramount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rdiscount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cashierorders`
--

INSERT INTO `cashierorders` (`id`, `cashier_id`, `item_id`, `rquantity`, `ramount`, `status`, `remember_token`, `created_at`, `updated_at`, `rdiscount`) VALUES
(1, '59', '1', '4', '544', '0', NULL, '2019-05-17 13:13:58', '2019-05-17 13:13:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tie Wire', NULL, '2019-03-14 00:55:00', '2019-03-14 00:55:00'),
(2, 'Deformed Steel Bar', NULL, '2019-03-14 00:55:31', '2019-03-14 00:55:31'),
(7, 'Paint', NULL, '2019-03-31 13:34:22', '2019-03-31 13:34:22'),
(8, 'Cyclone Wire(Interlink)', NULL, '2019-04-05 04:47:22', '2019-04-05 04:47:22'),
(9, 'Welding Rod', NULL, '2019-04-05 04:55:07', '2019-04-05 04:55:07'),
(10, 'Concrete Nails', NULL, '2019-04-05 04:58:42', '2019-04-05 04:58:42');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `boxId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactnum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchid` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dateactivation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custoorders`
--

CREATE TABLE `custoorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dailyquantitysales`
--

CREATE TABLE `dailyquantitysales` (
  `id` int(10) UNSIGNED NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salequantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saledate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dailyquantitysales`
--

INSERT INTO `dailyquantitysales` (`id`, `branchid`, `productid`, `salequantity`, `saledate`, `created_at`, `updated_at`) VALUES
(1, '5', '2', '5', '03-31-2019', '2019-03-31 13:58:26', '2019-03-31 13:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `distributionrecords`
--

CREATE TABLE `distributionrecords` (
  `id` int(10) UNSIGNED NOT NULL,
  `distributionnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recievequantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `distributionrecords`
--

INSERT INTO `distributionrecords` (`id`, `distributionnumber`, `branchid`, `productid`, `skuid`, `quantity`, `recievequantity`, `status`, `date`, `created_at`, `updated_at`) VALUES
(3, 'DR-03312019-3', '5', '1', '1', '2000', '2000', '2', '03/31/2019', '2019-03-31 08:08:10', '2019-03-31 08:11:42'),
(4, 'DR-03312019-4', '4', '2', '2', '20', NULL, '0', '03/31/2019', '2019-03-31 13:46:11', '2019-03-31 13:46:11'),
(5, 'DR-03312019-5', '5', '2', '2', '20', '20', '2', '03/31/2019', '2019-03-31 13:49:58', '2019-03-31 13:54:22');

-- --------------------------------------------------------

--
-- Table structure for table `distributions`
--

CREATE TABLE `distributions` (
  `id` int(10) UNSIGNED NOT NULL,
  `distributionnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `distributions`
--

INSERT INTO `distributions` (`id`, `distributionnumber`, `branchid`, `status`, `date`, `created_at`, `updated_at`) VALUES
(1, 'DR-03312019-0', '4', '0', '03/31/2019', '2019-03-31 08:04:35', '2019-03-31 08:04:35'),
(2, 'DR-03312019-0', '5', '0', '03/31/2019', '2019-03-31 08:05:35', '2019-03-31 08:05:35'),
(3, 'DR-03312019-3', '5', '2', '03/31/2019', '2019-03-31 08:07:38', '2019-03-31 08:11:45'),
(4, 'DR-03312019-4', '4', '1', '03/31/2019', '2019-03-31 13:45:44', '2019-03-31 13:46:26'),
(5, 'DR-03312019-5', '5', '2', '03/31/2019', '2019-03-31 13:49:45', '2019-03-31 13:54:46'),
(6, 'DR-03312019-6', '4', '0', '03/31/2019', '2019-03-31 14:17:23', '2019-03-31 14:17:23'),
(7, 'DR-04052019-7', '4', '0', '04/05/2019', '2019-04-05 05:08:03', '2019-04-05 05:08:03');

-- --------------------------------------------------------

--
-- Table structure for table `itembalances`
--

CREATE TABLE `itembalances` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unittype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(15, '2018_01_18_160417_create_categories_table', 2),
(16, '2018_01_18_160544_create_suppliers_table', 2),
(17, '2018_01_18_160558_create_brands_table', 2),
(18, '2018_01_18_160609_create_branchs_table', 2),
(19, '2018_01_18_160638_create_tags_table', 2),
(20, '2018_01_18_161337_create_items_table', 2),
(21, '2018_01_18_161354_create_requests_table', 2),
(23, '2018_01_18_161441_create_customers_table', 2),
(24, '2018_01_18_161519_create_customer_orders_table', 2),
(25, '2018_01_18_162425_create_item_balances_table', 2),
(26, '2018_01_18_162811_create_request_orders_table', 2),
(27, '2018_01_18_163332_create_order_items_table', 2),
(28, '2014_01_07_073615_create_tagged_table', 3),
(29, '2014_01_07_073615_create_tags_table', 3),
(30, '2016_06_29_073615_create_tag_groups_table', 3),
(31, '2016_06_29_073615_update_tags_table', 3),
(32, '2018_03_10_135411_create_product_table', 4),
(33, '2018_03_10_135650_create_product_quantities_table', 4),
(35, '2018_03_13_080222_create_cart_table', 5),
(36, '2018_03_15_071713_create_reservation_table', 5),
(37, '2018_03_16_051912_create_reservations_table', 6),
(38, '2018_03_16_093051_create_purchaserecord_table', 7),
(39, '2018_03_18_180012_create_cashierorder_table', 8),
(42, '2018_03_11_152907_create_purchase_table', 9),
(43, '2018_01_18_161405_create_orders_table', 10),
(44, '2018_05_26_044802_create_bookings_table', 11),
(45, '2018_05_28_134630_create_transports_table', 12),
(48, '2018_06_27_135404_create_purchaserequests_table', 13),
(49, '2018_08_09_162929_create_packages_table', 14),
(50, '2018_08_09_165808_create_packageitems_table', 15),
(51, '2018_08_10_174244_create_packagebranch_table', 16),
(52, '2018_08_10_174513_create_packagebranchs_table', 17),
(53, '2018_08_10_174602_create_packagebranches_table', 18),
(54, '2018_08_20_050155_create_distribution_table', 19),
(55, '2018_08_20_050213_create_distributionrecord_table', 19),
(56, '2018_09_13_154354_create_packageorder_table', 20),
(57, '2018_09_17_142749_create_dealer_table', 21),
(58, '2018_09_18_023623_create_boxid_table', 22),
(59, '2018_09_18_054926_create_dealerpackageorder_table', 23),
(60, '2018_10_18_160133_create_returns_table', 24),
(61, '2018_10_19_003636_create_returnproducts_table', 25),
(62, '2018_10_19_011327_create_returnproductlists_table', 26),
(63, '2018_10_26_233043_create_dailyquantitysale_table', 27),
(64, '2018_10_29_013003_create_cancelorder_table', 28),
(65, '2018_11_30_122647_create_productvariants_table', 29),
(66, '2018_11_30_122742_create_productvariantsoptions_table', 29),
(67, '2018_11_30_122841_create_skuproductvariantsoptions_table', 29),
(68, '2019_02_06_124919_create_productpictures_table', 30);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(10) UNSIGNED NOT NULL,
  `req_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `cashier_id` int(11) NOT NULL,
  `orderId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ornumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `itemId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ramount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rquantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rdiscount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `cashier_id`, `orderId`, `ornumber`, `itemId`, `ramount`, `rquantity`, `rdiscount`, `status`, `created_at`, `updated_at`) VALUES
(1, 59, 'G8ATTD0DE6', NULL, '2', '3200', '5', '0', 0, '2019-03-31 13:58:26', '2019-03-31 13:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `packageitems`
--

CREATE TABLE `packageitems` (
  `id` int(10) UNSIGNED NOT NULL,
  `packageid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productpictures`
--

CREATE TABLE `productpictures` (
  `id` int(10) UNSIGNED NOT NULL,
  `productid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productquantities`
--

CREATE TABLE `productquantities` (
  `id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(11) NOT NULL,
  `options_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `var_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productquantities`
--

INSERT INTO `productquantities` (`id`, `prod_id`, `options_id`, `brand_id`, `branch_id`, `price`, `description`, `quantity`, `unit`, `var_name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, '1', NULL, 5, '136', NULL, 2000, NULL, '\'[', NULL, '2019-03-31 07:48:20', '2019-03-31 08:11:42'),
(2, 2, '2', NULL, 5, '640.00', NULL, 15, NULL, 'Gloss Latex', NULL, '2019-03-31 13:44:12', '2019-03-31 13:58:26'),
(3, 2, '2', NULL, 9, '640.00', NULL, 0, NULL, 'Gloss Latex', NULL, '2019-03-31 13:51:00', '2019-03-31 13:51:00'),
(4, 3, '3', NULL, 5, '175.00', NULL, 0, NULL, 'Gloss Latex', NULL, '2019-03-31 14:08:21', '2019-03-31 14:08:21'),
(5, 4, '4', NULL, 5, '600.00', NULL, 0, NULL, 'Flat Wall Enamel', NULL, '2019-03-31 14:12:35', '2019-03-31 14:12:35'),
(6, 6, '5', NULL, 5, '165', NULL, 0, NULL, 'Enamel', NULL, '2019-04-05 04:27:56', '2019-04-05 04:27:56'),
(7, 7, '6', NULL, 5, '600', NULL, 0, NULL, 'Enamel', NULL, '2019-04-05 04:30:53', '2019-04-05 04:30:53'),
(8, 8, '7', NULL, 5, '670', NULL, 0, NULL, 'Enamel', NULL, '2019-04-05 04:33:08', '2019-04-05 04:33:08'),
(9, 9, '8', NULL, 5, '640', NULL, 0, NULL, 'Gloss Latex', NULL, '2019-04-05 04:35:14', '2019-04-05 04:35:14');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `pic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehousequantity` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bulkquantity` int(11) NOT NULL,
  `bulkunit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pic`, `product_name`, `description`, `brand_id`, `category_id`, `warehousequantity`, `remember_token`, `created_at`, `updated_at`, `unit`, `bulkquantity`, `bulkunit`) VALUES
(1, '', 'DSB 10mmx6m', 'Deform Steel Bar 10mmx6m', '1', '2', NULL, NULL, '2019-03-31 07:46:00', '2019-03-31 07:46:00', 'piece', 3000, 'Box'),
(2, '', 'Boysen Gloss Latex White Gal.', 'Color White', '2', '7', NULL, NULL, '2019-03-31 13:40:38', '2019-03-31 14:04:09', 'piece', 20, 'Box'),
(3, '', 'Boysen Gloss Latex White Liter', 'Liter', '2', '7', NULL, NULL, '2019-03-31 14:06:04', '2019-03-31 14:14:55', 'piece', 24, 'Box'),
(4, '', 'Boysen Flatwall Enamel White Gal.', 'Gallon', '2', '7', NULL, NULL, '2019-03-31 14:11:18', '2019-03-31 14:13:52', 'piece', 20, 'Box'),
(5, '', 'Boysen Flatwall Enamel White Liter', 'White - Liter', '2', '1', NULL, NULL, '2019-04-05 04:25:58', '2019-04-05 04:25:58', 'can', 24, 'Container'),
(6, '', 'Boysen Flatwall Enamel White Liter', 'White - Liter', '2', '7', NULL, NULL, '2019-04-05 04:26:06', '2019-04-05 04:26:06', 'can', 24, 'Container'),
(7, '', 'Boysen Semi Gloss Enamel White Gallon', 'White- Gallon', '2', '7', NULL, NULL, '2019-04-05 04:29:55', '2019-04-05 04:29:55', 'container', 12, 'Container'),
(8, '', 'Boysen Quick Dry Enamel White Gallon', 'White-Gallon', '2', '7', NULL, NULL, '2019-04-05 04:32:23', '2019-04-05 04:32:23', 'container', 12, 'Container'),
(9, '', 'Boysen Semi Gloss Latex White Gallon', 'Permacoat', '2', '7', NULL, NULL, '2019-04-05 04:34:28', '2019-04-05 04:34:28', 'container', 20, 'Container'),
(10, '', 'Boysen Flat Latex White Liter', 'White- Liter', '2', '7', NULL, NULL, '2019-04-05 04:40:13', '2019-04-05 04:40:13', 'can', 24, 'Set'),
(11, '', 'Boysen Flat Latex White Gallon', 'White - Gallon', '2', '7', NULL, NULL, '2019-04-05 04:42:25', '2019-04-05 04:42:25', 'container', 20, 'Container'),
(12, '', 'Boysen Plexibond Gallon', 'Gallon', '2', '7', NULL, NULL, '2019-04-05 04:45:13', '2019-04-05 04:45:13', 'container', 60, 'Container'),
(13, '', 'Cyclone Wire 4ft.', 'Interlink Wire 10x4', '5', '8', NULL, NULL, '2019-04-05 04:49:04', '2019-04-05 04:49:04', 'roll', 150, 'Set'),
(14, '', 'Cyclone Wire 5ft.', 'Interlink Wire 10x5', '5', '8', NULL, NULL, '2019-04-05 04:51:48', '2019-04-05 04:51:48', 'roll', 200, 'Set'),
(15, '', 'Golden Bridge Welding Rod', 'Golden Bridge 1/8', '1', '9', NULL, NULL, '2019-04-05 04:56:45', '2019-04-05 04:56:45', 'piece', 20, 'Box'),
(16, '', 'Concrete Nail 4\"', '25 kilos', '5', '10', NULL, NULL, '2019-04-05 05:00:54', '2019-04-05 05:00:54', 'piece', 10, 'Box'),
(17, '', 'Concrete Nail 3\"', '25 kilos', '5', '10', NULL, NULL, '2019-04-05 05:02:45', '2019-04-05 05:02:45', 'piece', 10, 'Box'),
(18, '', 'Cyclone Wire 6ft.', 'Interlink Wire 10x6', '5', '8', NULL, NULL, '2019-04-05 05:04:42', '2019-04-05 05:04:42', 'piece', 100, 'Set');

-- --------------------------------------------------------

--
-- Table structure for table `productvariants`
--

CREATE TABLE `productvariants` (
  `id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(11) NOT NULL,
  `var_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productvariants`
--

INSERT INTO `productvariants` (`id`, `prod_id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'safdkd;g', '2019-03-31 07:47:44', '2019-03-31 07:47:44'),
(2, 2, 'Boysen Latex Gloss White', '2019-03-31 13:43:31', '2019-03-31 13:43:31'),
(3, 3, 'Boysen Latex Gloss White Liter', '2019-03-31 14:08:10', '2019-03-31 14:08:10'),
(4, 4, 'Boysen Flat Wall Enamel White', '2019-03-31 14:12:29', '2019-03-31 14:12:29'),
(5, 6, 'BFWEW-L', '2019-04-05 04:27:46', '2019-04-05 04:27:46'),
(6, 7, 'BSGEW-Gal.', '2019-04-05 04:30:40', '2019-04-05 04:30:40'),
(7, 8, 'BQDEW-Gal.', '2019-04-05 04:32:58', '2019-04-05 04:32:58'),
(8, 9, 'BSGLW-Gal.', '2019-04-05 04:35:09', '2019-04-05 04:35:09'),
(9, 10, 'BFLW-Liter', '2019-04-05 04:41:08', '2019-04-05 04:41:08'),
(10, 10, 'BFLW-Liter', '2019-04-05 04:41:13', '2019-04-05 04:41:13'),
(11, 11, 'BFLW-Gal.', '2019-04-05 04:43:03', '2019-04-05 04:43:03'),
(12, 12, 'BP-Gal.', '2019-04-05 04:45:51', '2019-04-05 04:45:51'),
(13, 12, 'BP-Gal.', '2019-04-05 04:45:55', '2019-04-05 04:45:55'),
(14, 13, 'CW 4ft.', '2019-04-05 04:50:15', '2019-04-05 04:50:15'),
(15, 14, 'CW 5ft.', '2019-04-05 04:52:18', '2019-04-05 04:52:18'),
(16, 15, 'GB Welding Rod', '2019-04-05 04:57:43', '2019-04-05 04:57:43'),
(17, 16, 'CN 4\"', '2019-04-05 05:01:48', '2019-04-05 05:01:48'),
(18, 17, 'CN 3\"', '2019-04-05 05:03:14', '2019-04-05 05:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `productvariantsoptions`
--

CREATE TABLE `productvariantsoptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `var_id` int(11) NOT NULL,
  `option_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productvariantsoptions`
--

INSERT INTO `productvariantsoptions` (`id`, `var_id`, `option_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'DSB 10mmx6m', '2019-03-31 07:47:44', '2019-03-31 08:03:33'),
(2, 2, 'Gloss Latex', '2019-03-31 13:43:31', '2019-03-31 13:43:31'),
(3, 3, 'Gloss Latex', '2019-03-31 14:08:10', '2019-03-31 14:08:10'),
(4, 4, 'Flat Wall Enamel', '2019-03-31 14:12:29', '2019-03-31 14:12:29'),
(5, 5, 'Enamel', '2019-04-05 04:27:46', '2019-04-05 04:27:46'),
(6, 6, 'Enamel', '2019-04-05 04:30:40', '2019-04-05 04:30:40'),
(7, 7, 'Enamel', '2019-04-05 04:32:58', '2019-04-05 04:32:58'),
(8, 8, 'Gloss Latex', '2019-04-05 04:35:09', '2019-04-05 04:35:09'),
(9, 9, 'Latex', '2019-04-05 04:41:08', '2019-04-05 04:41:08'),
(10, 10, 'Latex', '2019-04-05 04:41:13', '2019-04-05 04:41:13'),
(11, 11, 'Latex', '2019-04-05 04:43:03', '2019-04-05 04:43:03'),
(12, 12, 'Plexibond', '2019-04-05 04:45:51', '2019-04-05 04:45:51'),
(13, 13, 'Plexibond', '2019-04-05 04:45:55', '2019-04-05 04:45:55'),
(14, 14, 'Cyclone', '2019-04-05 04:50:15', '2019-04-05 04:50:15'),
(15, 15, 'Cyclone', '2019-04-05 04:52:18', '2019-04-05 04:52:18'),
(16, 16, 'Welding Rod', '2019-04-05 04:57:43', '2019-04-05 04:57:43'),
(17, 17, 'C', '2019-04-05 05:01:48', '2019-04-05 05:01:48'),
(18, 18, 'C', '2019-04-05 05:03:14', '2019-04-05 05:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `purchaserecords`
--

CREATE TABLE `purchaserecords` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchasenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recievequantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodquantityid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `recievedate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchaserequests`
--

CREATE TABLE `purchaserequests` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchasenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchaserequests`
--

INSERT INTO `purchaserequests` (`id`, `purchasenumber`, `status`, `date`, `created_at`, `updated_at`) VALUES
(1, 'SST-01', '0', '04/05/2019', '2019-04-05 04:37:49', '2019-04-05 04:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amountpaid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `change` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cashier_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ornumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `amount`, `amountpaid`, `change`, `cashier_id`, `branchid`, `orderNumber`, `ornumber`, `date`, `status`, `created_at`, `updated_at`) VALUES
(1, '3200', '3200', '0', '59', '5', 'G8ATTD0DE6', '001', '2019-03-31', '0', '2019-03-31 13:58:26', '2019-03-31 13:58:26');

-- --------------------------------------------------------

--
-- Table structure for table `reqorders`
--

CREATE TABLE `reqorders` (
  `id` int(10) UNSIGNED NOT NULL,
  `req_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rquantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ramount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `req_order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(10) UNSIGNED NOT NULL,
  `requestId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reservestatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `returnsproductlists`
--

CREATE TABLE `returnsproductlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rquantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `returnbatchnum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `returnsproducts`
--

CREATE TABLE `returnsproducts` (
  `id` int(10) UNSIGNED NOT NULL,
  `branchid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rquantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skuproductvariantsoptions`
--

CREATE TABLE `skuproductvariantsoptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `var_id` int(11) NOT NULL,
  `options_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `warehousequantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `varprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skuproductvariantsoptions`
--

INSERT INTO `skuproductvariantsoptions` (`id`, `var_id`, `options_id`, `prod_id`, `warehousequantity`, `varprice`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '1000', '136', '2019-03-31 07:47:44', '2019-03-31 08:08:10'),
(2, 2, 2, 2, '0', '640.00', '2019-03-31 13:43:31', '2019-03-31 13:49:58'),
(3, 3, 3, 3, '24', '175.00', '2019-03-31 14:08:10', '2019-03-31 14:08:10'),
(4, 4, 4, 4, '20', '600.00', '2019-03-31 14:12:29', '2019-03-31 14:12:29'),
(5, 5, 5, 6, '24', '165', '2019-04-05 04:27:46', '2019-04-05 04:27:46'),
(6, 6, 6, 7, '12', '600', '2019-04-05 04:30:40', '2019-04-05 04:30:40'),
(7, 7, 7, 8, '12', '670', '2019-04-05 04:32:58', '2019-04-05 04:32:58'),
(8, 8, 8, 9, '20', '640', '2019-04-05 04:35:09', '2019-04-05 04:35:09'),
(9, 9, 9, 10, '24', '150', '2019-04-05 04:41:08', '2019-04-05 04:41:08'),
(10, 10, 10, 10, '24', '150', '2019-04-05 04:41:13', '2019-04-05 04:41:13'),
(11, 11, 11, 11, '20', '545', '2019-04-05 04:43:03', '2019-04-05 04:43:03'),
(12, 12, 12, 12, '60', '830', '2019-04-05 04:45:51', '2019-04-05 04:45:51'),
(13, 13, 13, 12, '60', '830', '2019-04-05 04:45:55', '2019-04-05 04:45:55'),
(14, 14, 14, 13, '150', '260', '2019-04-05 04:50:15', '2019-04-05 04:50:15'),
(15, 15, 15, 14, '300', '325', '2019-04-05 04:52:18', '2019-04-05 05:05:38'),
(16, 16, 16, 15, '20', '65', '2019-04-05 04:57:43', '2019-04-05 04:57:43'),
(17, 17, 17, 16, '30', '90', '2019-04-05 05:01:48', '2019-04-05 05:06:44'),
(18, 18, 18, 17, '15', '90', '2019-04-05 05:03:14', '2019-04-05 05:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactnum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilepic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `usertype`, `contactnum`, `company`, `profilepic`, `remember_token`, `created_at`, `updated_at`) VALUES
(21, 'Gege', 'gegejosper@gmail.com', '$2y$10$CcMDgpcyfEhZhcrzEMgkGO7asknQ.dqxFjipDPFUSAMQw6ZjqCmIa', 'admin', NULL, NULL, '', 'HQI9JbQ16LrVFtp1dyMZ3Lyk2pv6yCwKJqWLUg72wSs2L0EplpFCEzl9xZwd', '2018-01-22 05:52:13', '2018-01-22 05:52:13'),
(54, 'Jojo', 'jojo@sst-portal.com', '$6$roottn$lCukmfCJGtLN.vP9WSQlpcTSNYNHKz81YAmbxW/iuZ7cZD4AYt7AjnX.FR1F/lC2SSM3P5hfQsM811Qgk85iN/:16249:::::', 'checker', NULL, NULL, NULL, 'ILu51isl4xz5PdzbQZch8wzR7lLLVB2zpDgKPOGQ7PZe4c7Cj4rDjiaz2bMx', '2019-03-14 01:26:01', '2019-03-14 01:57:49'),
(55, 'Helen', 'helen@sst-portal.com', '$2y$10$balarGLf9kx5UmGmLGBJi.5gCXwSuaKNcYI10wCyHp/kzz3qK.EHq', 'cashier', NULL, NULL, NULL, 'II8jmG1BegZEYjmMmqztniDXQGoWBngGEs2ydBItSdf5v3mz5g8yJVDPP65I', '2019-03-14 01:26:45', '2019-05-17 13:08:44'),
(57, 'Bencito', 'bencito@sst-portal.com', '$2y$10$2d67g24l/5kbsYf70s6Goeva68cL4W38V3JueeyQb5KNdQM1UEOd.', 'admin', NULL, NULL, NULL, 'SlHfSXAjQMBLtanR0qjsRgS1bu1xDDfeC3kkXIs2e1wzDH398mOstLlWce3j', '2019-03-14 01:52:32', '2019-03-14 01:52:32'),
(58, 'Mike', 'mike@sst-portal.com', '$2y$10$yHO2Z3V9YBlEWsFLWdpNXeBWtp2DIIvX4yvRW525tO3yl/UQxuTF2', 'checker', NULL, NULL, NULL, 'ovxofRZ2EmXLcWBA3wMtEN39TE0yW7s5C6yKM42cLbM9YXDdaQCFBcLna1rp', '2019-03-14 02:58:14', '2019-05-17 13:03:23'),
(59, 'nico', 'nico@sst-portal.com', '$2y$10$oyMQuS7xcHHO1Bwcj55B1.ieCP2J8JGoTRNfnS35.9nG6zUC2.Q6C', 'cashier', NULL, NULL, NULL, 'Dju5Vsvq56dl5GruNNxIWeE2mFYrcWpvQzYDBNvc6E6E8AHdHs9KGAqdvttq', '2019-03-14 02:58:43', '2019-05-17 13:11:38'),
(60, 'jun', 'jun@sst-portal.com', '$2y$10$ZlveCLSmqHTyDK2nf.IsOuG6/H2DjWGgFyABRh3Q6UnA9QvmYToEe', 'checker', NULL, NULL, NULL, 'mJY6QyIy9qlsfUCU1Yn9qXlv4ifH6yln6oXtj8UYjSj2FAQbJRyDSqdN9rDW', '2019-03-14 03:08:53', '2019-03-14 03:08:53'),
(61, 'jane', 'jane@sst-portal.com', '$2y$10$UsjkO5pwS3G9AXy2Hy1GKeWt91yE8TnYXK3h8h/lOn4/P.gcmhsCy', 'cashier', NULL, NULL, NULL, 'kzCSvOjquW23qxYkbk0D6jEPEIdbw3AG1zHPzr9hMJ763SLO1FBedzvbaGs9', '2019-03-14 03:09:09', '2019-03-14 03:09:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branchusers`
--
ALTER TABLE `branchusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cancelorders`
--
ALTER TABLE `cancelorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashierorders`
--
ALTER TABLE `cashierorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custoorders`
--
ALTER TABLE `custoorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dailyquantitysales`
--
ALTER TABLE `dailyquantitysales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributionrecords`
--
ALTER TABLE `distributionrecords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributions`
--
ALTER TABLE `distributions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itembalances`
--
ALTER TABLE `itembalances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packageitems`
--
ALTER TABLE `packageitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productpictures`
--
ALTER TABLE `productpictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productquantities`
--
ALTER TABLE `productquantities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productvariants`
--
ALTER TABLE `productvariants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productvariantsoptions`
--
ALTER TABLE `productvariantsoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchaserecords`
--
ALTER TABLE `purchaserecords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchaserequests`
--
ALTER TABLE `purchaserequests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reqorders`
--
ALTER TABLE `reqorders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returnsproductlists`
--
ALTER TABLE `returnsproductlists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `returnsproducts`
--
ALTER TABLE `returnsproducts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skuproductvariantsoptions`
--
ALTER TABLE `skuproductvariantsoptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `branchusers`
--
ALTER TABLE `branchusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cancelorders`
--
ALTER TABLE `cancelorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashierorders`
--
ALTER TABLE `cashierorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custoorders`
--
ALTER TABLE `custoorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dailyquantitysales`
--
ALTER TABLE `dailyquantitysales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `distributionrecords`
--
ALTER TABLE `distributionrecords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `distributions`
--
ALTER TABLE `distributions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `itembalances`
--
ALTER TABLE `itembalances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `packageitems`
--
ALTER TABLE `packageitems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productpictures`
--
ALTER TABLE `productpictures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productquantities`
--
ALTER TABLE `productquantities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `productvariants`
--
ALTER TABLE `productvariants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `productvariantsoptions`
--
ALTER TABLE `productvariantsoptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `purchaserecords`
--
ALTER TABLE `purchaserecords`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchaserequests`
--
ALTER TABLE `purchaserequests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reqorders`
--
ALTER TABLE `reqorders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `returnsproductlists`
--
ALTER TABLE `returnsproductlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `returnsproducts`
--
ALTER TABLE `returnsproducts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skuproductvariantsoptions`
--
ALTER TABLE `skuproductvariantsoptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
