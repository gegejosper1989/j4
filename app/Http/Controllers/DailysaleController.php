<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Dailysale;
use App\Productquantity;

class DailysaleController extends Controller
{
    //

    public function closedailysale(Request $req){

        $dataSale = new Dailysale();
        $dataSale->dailysales = $req->dailysales;
        $dataSale->grossdailysales = $req->totaldailysales;
        $dataSale->grossprofit = $req->grossprofit;
        $dataSale->expenses = $req->expenses;
        $dataSale->netprofit = $req->netprofit;
        $dataSale->save();

        return response()->json($dataSale);

    }

    public function dailysale($dailysaleid){
       
        $dataSale = Dailysale::where('id', '=', $dailysaleid)->first();
        //dd($dataSale);
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.sale', compact('dataSale', 'countOutOfStock'));

    }
}
