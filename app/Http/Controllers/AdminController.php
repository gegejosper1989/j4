<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Supplier;
use App\Category;
use App\Brand;
use App\Branch;
use App\Productquantity;
use App\Deliveryrecord;
use App\Product;
use App\Purchase;
use App\Dailysale;
use App\Returnsproductlist;
use App\Distribution;
use App\Purchaserecord;
use App\Purchaserequest;
Use DB;

use App\Setting;
use App\User;

class AdminController extends Controller
{
    //
    public function index(){
        $TotalSales = Purchase::sum('amount');
        $DailySales = Purchase::where('date', '=', date('Y-m-d'))
        ->sum('amount');
        $reportPurchase = Purchase::with('cashier')->take(5)->latest()->get();
        //$dataProduct = Productquantity::where('quantity','<', 10)->with('product', 'branch')->take(10)->get();
        $reportDailysale = Dailysale::whereDate('created_at', '=', date('Y-m-d'))
                                    ->count();
        //dd($reportDailysale);
        $dataPurchase = Purchaserecord::groupBy('purchasenumber')->orderBy('date', 'DESC')->take(10)->get();
        // $reportPackageorder = Packageorder::take(10)->latest()->get();
        $dataProduct = Product::with('distributionrecord', 'productquantities.variation', 'productskus.varoption')->orderBy('product_name', 'asc')->take(10)->get();
        $dataBranch = Branch::with('products')->get();
        $dataSetting = Setting::first();
        $dataOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->take(10)->get();
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        //dd($countOutOfStock);
        $dataDistributionList = Distribution::latest()->with('branch')->take(5)->get();
      
        return view('admin.dashboard', compact('dataPurchase','dataDistributionList','DailySales','TotalSales','dataProduct', 'reportPurchase','dataBranch', 'dataSetting', 'reportDailysale', 'dataOutOfStock', 'countOutOfStock'));
    }
    public function shop(){
        return view('admin.shop');
    }

    public function outofstocks() {
        
        $dataBranch = Branch::where('id','=',4)->get();
        $dataBranchId = 4;
        $id = 4;    
        $dataProduct = Product::orderBy('product_name', 'asc')->get();
        $arrayProduct = array();
        foreach ($dataProduct as $Product) {
            $dataQuantity = Productquantity::where('branch_id','=',$id)
            ->where('prod_id', '=',$Product->id)
            ->get();
            if ($dataQuantity->count() == 0) {
                $arrayProduct[] = $Product;
            }
        }
        
        $dataBranchProduct = Productquantity::where('branch_id','=',$id)->with('product')->with('productvariants')->get();
        $dataBrand = Brand::all();
        $dataCategory = Category::all();
        $dataSupplier = Supplier::all();
        //dd($dataBranchProduct);
        $dataOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->get();
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.outofstocks', compact('arrayProduct','dataBrand','dataCategory','dataBranch','dataSupplier', 'dataBranchProduct', 'data', 'dataBranchId', 'dataOutOfStock', 'countOutOfStock'));
    }
    public function request(){
        return view('admin.request');
    }
    public function login()
    {
        return view('admin.login');
    }

    public function members(){
        $dataUser = User::where('usertype', '=', 'customer')->get();
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.members',compact('dataUser', 'countOutOfStock'));
    }

    public function report()
    {
        $dataBranch = Branch::get();
        $reportPurchase = Purchase::where('date', '=', date('Y-m-d'))->with('cashier')->get();
        $TotalSalesWalkin = Purchase::sum('amount');
      
        $TotalSales = $TotalSalesWalkin;
        
        $TotalYearlyWalkin = Purchase::whereYear('created_at', date('Y'))->sum('amount');
        $TotalYearlySales =  $TotalYearlyWalkin ;
        $TotalMonthlyWalkin = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        $TotalMonthlySales =  $TotalMonthlyWalkin ;
        $DailySalesWalkin = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        
        $DailySales = $DailySalesWalkin;
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        $dailysale = Dailysale::take(10)->latest()->get();
        return view('admin.reports',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales', 'TotalYearlySales','TotalMonthlySales', 'dailysale', 'countOutOfStock'));
    }
    public function monthreport(Request $req)
    {
        $dataBranch = Branch::get();
        $reportPurchase = Purchase::where('date', '=', date('Y-m-d'))->with('cashier')->get();
        $TotalSalesWalkin = Purchase::sum('amount');
      
        $TotalSales = $TotalSalesWalkin;
        
        $TotalYearlyWalkin = Purchase::whereYear('created_at', date('Y'))->sum('amount');
        $TotalYearlySales =  $TotalYearlyWalkin ;
        $TotalMonthlyWalkin = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        $TotalMonthlySales =  $TotalMonthlyWalkin ;
        $DailySalesWalkin = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        
        $DailySales = $DailySalesWalkin;
        $dailysale = Dailysale::take(10)->latest()->get();
        $dailysaleMonthly = Dailysale::whereYear('created_at', '=', $req->year)
        ->whereMonth('created_at', '=', $req->month)->latest()->get();
        $Year = $req->year;
        $Month = $req->month;
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.report-month',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales', 'TotalYearlySales','TotalMonthlySales', 'dailysale', 'dailysaleMonthly', 'Month', 'Year', 'countOutOfStock'));
    }
    public function reportDelivery(){
        $dataBranch = Branch::get();
        $reportPurchase = Purchase::where('date', '=', date('Y-m-d'))->with('cashier')->get();
        
       
        $TotalSalesWalkin = Purchase::sum('amount');
      
        $TotalSales = $TotalSalesWalkin;
        
        
        $TotalYearlyWalkin = Purchase::whereYear('created_at', date('Y'))->sum('amount');
       
        $TotalYearlySales =  $TotalYearlyWalkin ;

        $TotalMonthlyWalkin = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        
        $TotalMonthlySales =  $TotalMonthlyWalkin ;

        $DailySalesWalkin = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        
        $DailySales = $DailySalesWalkin ;
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        $dataDelivery = Deliveryrecord::where('status', '=', 'FINAL')->take(50)->latest()->get();
        return view('admin.report-delivery',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales', 'TotalYearlySales','TotalMonthlySales', 'dataDelivery', 'countOutOfStock'));
    }
    public function vieworder($ordernumber)
    {

        $reportPurchase = Purchase::where('orderNumber', '=',$ordernumber )->first();
        //$orderDetails = Order::where('orderId', '=', $ordernumber)->get();
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        $orderDetails = DB::table('orders')
        ->join('productquantities', 'orders.itemId', '=', 'productquantities.id')
        ->join('products', 'productquantities.prod_id', '=', 'products.id')
        ->where('orderId', '=', $ordernumber)
        ->get();
        //dd($orderDetails);
        return view('admin.vieworder',compact('reportPurchase', 'orderDetails'));
    }
    public function packagesreport()
    {
        $dataBranch = Branch::get();
       
       
       
        $TotalSalesWalkin = Purchase::sum('amount');
     

        $TotalSales = $TotalSalesWalkin;
        
        
        $TotalYearlyWalkin = Purchase::whereYear('created_at', date('Y'))->sum('amount');
       
        $TotalYearlySales =  $TotalYearlyWalkin;

        $TotalMonthlyWalkin = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        
        $TotalMonthlySales =  $TotalMonthlyWalkin;

        $DailySalesWalkin = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        
        $DailySales = $DailySalesWalkin;
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.packagesreport',compact('dataBranch', 'TotalSales', 'DailySales','TotalYearlySales','TotalMonthlySales', 'countOutOfStock'));
    }

    public function salesreport()
    {
        $dataBranch = Branch::get();
        $reportPurchase = Purchase::where('date', '=', date('Y-m-d'))->get();
       
               
        $TotalSales = Purchase::sum('amount');
        
        $TotalYearlySales = Purchase::whereYear('created_at', date('Y'))->sum('amount');

        $TotalMonthlySales = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        
        $DailySales = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.salesreport',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales','TotalYearlySales','TotalMonthlySales', 'countOutOfStock'));
    
    }
    public function reportsalesrange(Request $request){
        $dataBranch = Branch::get();
       
        $reportPurchase = Purchase::where('date', '>=', $request->from)
        ->where('date', '<=', $request->to)
        ->get();


        $TotalSalesWalkin = Purchase::sum('amount');
        
       
        $TotalSales = $TotalSalesWalkin;
        
        
        $TotalYearlyWalkin = Purchase::whereYear('created_at', date('Y'))->sum('amount');
       
        $TotalYearlySales =  $TotalYearlyWalkin;

        $TotalMonthlyWalkin = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        
        $TotalMonthlySales =  $TotalMonthlyWalkin;

        $DailySalesWalkin = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        $DailySales = $DailySalesWalkin;
        return view('admin.salesreport',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales', 'TotalYearlySales','TotalMonthlySales', 'countOutOfStock'));
    }
    
    public function reportrange(Request $request){
        
        $fromdate = Carbon::parse($request->from.' 00:00:00');
        $todate = Carbon::parse($request->to .' 23:59:59'); 
        $dataBranch = Branch::get();

        
        $reportPurchase = Purchase::where('date', '>=', $request->from)
        ->where('date', '<=', $todate)
        ->get();


        //dd($request);
        $TotalSalesWalkin = Purchase::sum('amount');
       
        
        $TotalSales = $TotalSalesWalkin;
        
        
        $TotalYearlyWalkin = Purchase::whereYear('created_at', date('Y'))->sum('amount');
        
        $TotalYearlySales =  $TotalYearlyWalkin;

        $TotalMonthlyWalkin = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
       
        $TotalMonthlySales =  $TotalMonthlyWalkin;

        $DailySalesWalkin = Purchase::where('date', '=', date('Y-m-d'))->sum('amount');
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        $DailySales = $DailySalesWalkin;
        return view('admin.reports',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales','TotalYearlySales','TotalMonthlySales', 'countOutOfStock'));
    }


    public function reportrangebranch(Request $request){
        
        $fromdate = Carbon::parse($request->from.' 00:00:00');
        $todate = Carbon::parse($request->to .' 23:59:59'); 
        $branchid = $request->branch;
        $dataBranch = Branch::get();
        
        $reportPurchase = Purchase::where('date', '>=', $request->from)
                        ->where('date', '<=', $todate)
                        ->where('branchid', '<=', $request->branch)
                        ->get();
      

        //dd($request);
        $TotalSalesWalkin = Purchase::where('branchid', '<=', $request->branch)->sum('amount');
       
        $TotalSales = $TotalSalesWalkin;
        
        
        $TotalYearlyWalkin = Purchase::where('branchid', '<=', $request->branch)->whereYear('created_at', date('Y'))->sum('amount');
       
        $TotalYearlySales =  $TotalYearlyWalkin;

        $TotalMonthlyWalkin = Purchase::where('branchid', '<=', $request->branch)->whereMonth('created_at', '=', date('m'))->sum('amount');
        
        $TotalMonthlySales =  $TotalMonthlyWalkin;

        $DailySalesWalkin = Purchase::where('branchid', '<=', $request->branch)->where('date', '=', date('Y-m-d'))->sum('amount');
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        $DailySales = $DailySalesWalkin;
        return view('admin.reportsbranch',compact('dataBranch','reportPurchase', 'TotalSales', 'DailySales','TotalYearlySales','TotalMonthlySales', 'branchid', 'countOutOfStock'));
    }

    public function reportbranch($branchid){

        $getBranch = Branch::where('id', '=',$branchid)->first();
        $dataBranch = Branch::get();
       
        $reportPurchase = Purchase::where('branchid','=', $branchid)
                                    ->where('created_at', '>=', date('Y-m-d'))
                                    ->get();
        $TotalSales = Purchase::sum('amount');
        $TotalYearlySales = Purchase::whereYear('created_at', date('Y'))->sum('amount');
        $TotalMonthlySales = Purchase::whereMonth('created_at', '=', date('m'))->sum('amount');
        $DailySales = Purchase::where('date', '=', date('Y-m-d'))
        ->sum('amount');
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.reportsbranch',compact('reportPurchase', 'dataBranch' ,'TotalSales', 'DailySales','TotalYearlySales','TotalMonthlySales', 'branchid', 'countOutOfStock'));
    }
    public function settings(){
        
        $dataCategory = Category::take(10)->get();
        $dataSupplier = Supplier::take(10)->get();
        $dataBrand = Brand::take(10)->get();
        
        $dataBranch = Branch::with('cashier')->take(10)->get();
        //dd($dataBranch);
        $dataUser = User::take(10)->get();
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.settings',compact('dataCategory', 'dataSupplier', 'dataBrand', 'dataUser', 'dataBranch', 'countOutOfStock'));
    }
    public function categories(){
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.categories', compact('countOutOfStock'));
    }
    public function users(){
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.users', compact('countOutOfStock'));
    }
    public function suppliers(){
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.suppliers', compact('countOutOfStock'));
    }
    public function brands(){
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.brands', compact('countOutOfStock'));
    }


    public function adminvieworder($ordernumber)
    {

        $reportPurchase = Purchase::where('orderNumber', '=',$ordernumber )->first();
        //$orderDetails = Order::where('orderId', '=', $ordernumber)->get();
        // $userId = Auth::user()->id;
        // $dataBranch = Branchuser::where('userid', '=', $userId)->first();
        $orderDetails = DB::table('orders')
        ->join('productquantities', 'orders.itemId', '=', 'productquantities.id')
        ->join('products', 'productquantities.prod_id', '=', 'products.id')
        ->join('users', 'orders.cashier_id', '=', 'users.id')
        ->where('orderId', '=', $ordernumber)
        ->get();
        //dd($orderDetails);
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.viewadminorder',compact('reportPurchase', 'orderDetails', 'countOutOfStock'));
    }

    public function returnslist(){
        //$reportPurchase = Purchase::where('orderNumber', '=',$ordernumber )->first();
        
        $returnDetails = Returnsproductlist::with('branch')
        ->groupBy('returnbatchnum')
        ->get();
        //dd($returnDetails);
        //return response()->json();
        return view('admin.returnslist',compact('returnDetails', 'countOutOfStock'));
    }
    public function returnnum($returnbatchnum){
        //$reportPurchase = Purchase::where('orderNumber', '=',$ordernumber )->first();
        $returnDetails = DB::table('returnsproductlists')
        ->join('productquantities', 'returnsproductlists.item_id', '=', 'productquantities.id')
        ->join('products', 'productquantities.prod_id', '=', 'products.id')
        ->where('returnbatchnum', '=', $returnbatchnum)
        
        ->get();
        
        //return response()->json();
        $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
        return view('admin.viewreturn',compact('returnDetails', 'returnbatchnum', 'countOutOfStock'));
    }
}
