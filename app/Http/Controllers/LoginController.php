<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    public function adminLogin(Request $request){
        //dd($request->all());
        
        if(Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])){
            $user = User::where('username', $request->username )->first();
        
            if($user->usertype=='admin'){
                return redirect('admin/dashboard');
               
            }
            else {
                return redirect('/');
            }
        }
        else {
            //return('error');
            return redirect()->back()->with('error', 'Login Credentials Incorrect!');
        }
        //return redirect('dashboard');
    }
    public function cashierLogin(Request $request){
        //dd($request->all());
        
        if(Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])){
            $user = User::where('username', $request->username )->first();
        
            if($user->usertype=='cashier'){
                return redirect('cashier/dashboard');
               
            }
            else {
                //return redirect('cashier/dashboard');
                return redirect('/');
            }
        }
        else {
            //return('error');
            return redirect()->back()->with('error', 'Login Credentials Incorrect!');
        }
    }
    
    public function checker()
    {
        return view('checker.login');
    }
    public function cashier()
    {
        return view('cashier.login');
    }
    public function accounting()
    {
        return view('accounting.login');
    }
    public function oic()
    {
        return view('oic.login');
    }
    public function assistant()
    {
        return view('assistant.login');
    }
    public function customer()
    {
        return view('customer.login');
    }
}
