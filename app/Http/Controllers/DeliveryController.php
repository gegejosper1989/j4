<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Productquantity;
use App\Skuproductvariantsoption;
use App\Deliveryrecord;
use App\Deliverydetail;


class DeliveryController extends Controller
{
    //

    public function delivery()
    {

            $dataDeliverydetail = Deliverydetail::where('status', '=', 'INITIAL')->with('product', 'variation')->orderBy('id', 'DESC')->get();

            $dataProduct = Product::with('productvariation.productvariations', 'productskus.varoption')->latest()->paginate(25);
            //dd($dataDeliverydetail);
            $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
            return view('admin.delivery',compact('dataDeliverydetail', 'dataProduct', 'countOutOfStock')); 
    }

    public function deliveryDetails($deliverynum)
    {

            $dataDeliverydetail = Deliverydetail::where('deliverynum','=',$deliverynum)->with('product', 'variation')->get();
            $countOutOfStock = Productquantity::whereColumn('quantity', '<=','orderingpoint')->with('product', 'productvariants')->count();
            return view('admin.deliverydetails',compact('dataDeliverydetail', 'deliverynum', 'countOutOfStock')); 
    }

    public function addDeliveryRecord(Request $request)
    {
            $record = Deliveryrecord::count();
            if($record == 0){
                $deliveryRecord = "DR-".date('Y')."-".date('m').'-1';  
            }
            else {
                $recordStat = Deliveryrecord::where('status', '=', 'INITIAL')->count();
                if($recordStat == 0){
                    $deliveryRecord = "DR-".date('Y')."-".date('m')."-".$record;
                    $recData = new Deliveryrecord();
                    $recData->deliverynum = $deliveryRecord;
                    $recData->deliverydate = date("m-d-Y");
                    $recData->status = 'INITIAL';
                    $recData->save();
                }
                else {
                    $recordLastNumber = Deliveryrecord::where('status', '=', 'INITIAL')->first();
                    $deliveryRecord = $recordLastNumber->deliverynum;
                }
                
            }

            $data = new Deliverydetail();
            $data->deliverynum = $deliveryRecord;
            $data->productquantityid = $request->prodquantityid;
            $data->skuid = $request->sku_id;
            $data->quantity = $request->quantity;
            $data->status = 'INITIAL';
            $data->save();
           
            return response()->json();
    }

    public function saveDelivery(){
        

        $getDeliveryDetail = Deliverydetail::where('status', '=', 'INITIAL')
            ->get();

        foreach ($getDeliveryDetail as $DeliveryDetail){
            $getProductquantity = Productquantity::where('prod_id', '=', $DeliveryDetail->productquantityid)
                ->where('options_id', '=', $DeliveryDetail->skuid)->first();
            $newQuantity = $getProductquantity->quantity + $DeliveryDetail->quantity;
            
            $updateDeliveryDetail = Deliverydetail::where('id', '=', $DeliveryDetail->id)
                ->update(['status' => 'FINAL']);

            $updateProductquantity = Productquantity::where('id', '=', $getProductquantity->id)
                ->update(['quantity' => $newQuantity]);

          $deliverynum = $DeliveryDetail->deliverynum;
        }
        $updateDeliveryRecord = Deliveryrecord::where('deliverynum', '=', $deliverynum)
            ->update(['status' => 'FINAL']);
        return redirect('/admin/delivery/details/'.$deliverynum);
    }

    public function deletedeliveryrecord(Request $req){
        Deliverydetail::find($req->id)->delete();
        return response()->json();
    }
}
