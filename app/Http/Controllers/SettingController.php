<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Setting;

class SettingController extends Controller
{
    //
    public function percentSave(Request $req){
        $countSetting = Setting::count();
        if($countSetting !=0){
            Setting::where('id', '=', $req->setid)
            ->update(['percentagesale' => $req->percentval]);

            return response()->json();
        }
        else {
            $dataSetting = new Setting();
            $dataSetting->percentagesale = $req->percentval;
            $dataSetting->save();
            return response()->json();
        }
        
    }   
}
