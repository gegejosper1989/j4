<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliverydetail extends Model
{
    //

    public function product()
    {
        return $this->belongsTO('App\Product', 'productquantityid', 'id');
    }
    public function variation()
    {
        return $this->belongsTO('App\Productvariantsoption', 'skuid', 'id');
    }
}
